<?php

namespace Yadda\Enso\Utilities\Traits;

use Illuminate\Support\Collection;

trait SharesBodyClasses
{
    /**
     * This should be the name of a property that holds
     * a Collection to user to store and manipulate body
     * classes.
     *
     * @var string
     */
    protected $body_class_property = 'body_classes';

    /**
     * The variable in which the body classes will be shared
     * with a view.
     *
     * @var string
     */
    protected $body_class_variable = 'body_classes';

    /**
     * Gets the current body class list
     *
     * @return Collection
     */
    protected function getBodyClasses()
    {
        $this->maybeSetupBodyClasses();

        return $this->{$this->body_class_property};
    }

    /**
     * Helper wrapper for shorted syntax, either gets the body classes
     * or adds to the current list, depending on the argument.
     *
     * @param Collection|array|string|null
     * 
     * @return self|Collection
     */
    protected function bodyClasses($argument = null)
    {
        if (is_null($argument)) {
            return $this->getBodyClasses();
        }

        $this->addBodyClasses($argument);

        return $this;
    }

    /**
     * Adds body classes to the current list, if not already present. Accepts either a collection
     * of class names, an array of class names or a single class name.
     *
     * @param  Collection|array|string $body_classes
     * 
     * @return self
     */
    protected function addBodyClasses($body_classes)
    {
        $this->maybeSetupBodyClasses();

        $this->{$this->body_class_property} = $this->getBodyClasses()->merge($body_classes)->unique();

        $this->shareBodyClasses();

        return $this;
    }

    /**
     * Adds a body class to the current list, if not already present
     *
     * @deprecated - Just use addBodyClasses, it now accepts a string as a parameter
     * 
     * @param  string       $body_class         Body class to add
     * @return self
     */
    protected function addBodyClass(string $body_class)
    {
        $this->maybeSetupBodyClasses();

        if (!$this->getBodyClasses()->contains($body_class)) {
            $this->getBodyClasses()->push($body_class);
        }

        $this->shareBodyClasses();

        return $this;
    }

    /**
     * Removes body classes from the current list, if present. Accepts either a collection
     * of class names, an array of class names, or a single class name.
     *
     * @param  Collection|array|string $body_classes
     * 
     * @return self
     */
    protected function removeBodyClasses($body_classes)
    {
        $this->maybeSetupBodyClasses();

        $this->{$this->body_class_property} = $this->getBodyClasses()->diff($body_classes);

        $this->shareBodyClasses();

        return $this;
    }

    /**
     * Removes a body class from the current list, if present
     *
     * @deprecated - Just use removeBodyClasses, it now accepts a string as a parameter
     * 
     * @param  string       $body_class         Body class to remove
     * @return self
     */
    protected function removeBodyClass(string $body_class)
    {
        $this->maybeSetupBodyClasses();

        if ($this->getBodyClasses()->contains($body_class)) {
            $this->{$this->body_class_property} = $this->getBodyClasses()->filter(function($class) use ($body_class) {
                return $class !== $body_class;
            });
        }

        $this->shareBodyClasses();

        return $this;
    }

    /**
     * Sets default body classes based on the current url. If url is '/', set
     * single body class of 'homepage'.
     *
     * @return self
     */
    protected function setBodyClassesByUrl()
    {
        $this->maybeSetupBodyClasses();

        $url_path = request()->path();

        if($url_path === '/') {
            $this->addBodyClasses(['homepage']);
        } else {
            $this->addBodyClasses(explode('/', $url_path));
        }

        $this->shareBodyClasses();

        return $this;
    }

    /**
     * If a collection has not yet been set up to house body classes,
     * set one up
     *
     * @return void
     */
    protected function maybeSetupBodyClasses()
    {
        if (!property_exists($this, $this->body_class_property)) {
            $this->{$this->body_class_property} = new Collection;
        }
    }

    /**
     * Shares the current body class list. This will overwrite any previously
     * shared version.
     *
     * @return self
     */
    protected function shareBodyClasses()
    {
        $this->maybeSetupBodyClasses();
        
        view()->share($this->body_class_variable, $this->getBodyClasses());

        return $this;
    }
}
