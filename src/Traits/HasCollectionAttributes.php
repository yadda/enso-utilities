<?php

namespace Yadda\Enso\Utilities\Traits;

use Exception;
use Illuminate\Support\Collection;

/**
 * This trait allows you to keep one or more Collections of mixed, un-indexed
 * items with the ability to order and move items within the collection.
 *
 * You should set up the Collection(s) in the __construct function of the
 * class that uses this Trait.
 */
trait HasCollectionAttributes
{
    /**
     * Override this function to determine how to get the identifier from items
     * that are or can be put in a collection.
     *
     * The property name is provided in case you are using multple CollectionAttributes
     * that have different requirements.
     *
     * @param string $property_name
     * @param mixed $item
     *
     * @return mixed
     */
    protected function getCollectionAttributeValueIdentifier($property_name, $item)
    {
        return $item;
    }

    /**
     * Returns a callable that can be passed into Collection modification
     * calls.
     *
     * @param string $property_name
     * @param mixed $identifier
     *
     * @return callable
     */
    protected function collectionAttributeSearch($property_name, $identifier)
    {
        return function ($item) use ($property_name, $identifier) {
            return $this->getCollectionAttributeValueIdentifier($property_name, $item) === $identifier;
        };
    }

    /**
     * Ensures that a new value does not already exist within the underlying Collection in
     * the given property name by throwing an exception if it is.
     *
     * @param string $property_name
     * @param mixed $item
     *
     * @return void
     */
    protected function ensureCollectionAttributeIsNotPresent($property_name, $new_value)
    {
        $identifier = $this->getCollectionAttributeValueIdentifier($property_name, $new_value);

        if ($this->hasCollectionAttributeValue($property_name, $identifier)) {
            throw new Exception("Cannot add Collection Attribute Value to `{$property_name}` with duplicate name: `$identifier`");
        }
    }

    /**
     * Gets the current index of a value with the given identifier.
     *
     * @param string $property_name
     * @param mixed $identifier
     *
     * @return integer|false
     */
    protected function getCollectionAttributeValueCurrentIndex($property_name, $identifier)
    {
        return $this->$property_name->search($this->collectionAttributeSearch($property_name, $identifier));
    }

    /**
     * Inserts values into the underlying Collection instance, normalising the
     * provided values.
     *
     * This has been done as empty Collections and empty arrays do not give
     * the same results when splicing into Collections.
     *
     * @param string $property_name
     * @param integer $index
     * @param mixed $values
     *
     * @return self
     */
    protected function insertValues($property_name, $index, $values)
    {
        if ($values instanceof Collection) {
            $values = $values->all();
        }

        $this->$property_name->splice($index, 0, $values);

        return $this;
    }

    /**
     * Gets the underlying Collection in the given property
     *
     * @param string $property_name
     *
     * @return Collection
     */
    protected function getCollectionAttributeValues($property_name)
    {
        return $this->$property_name;
    }

    /**
     * Gets a value from the underlying Collection in the given property name,
     * based on a given identifier.
     *
     * @param string $property_name
     * @param mixed $identifier
     *
     * @return mixed
     */
    protected function getCollectionAttributeValue($property_name, $identifier)
    {
        return $this->getCollectionAttributeValues($property_name)
            ->first($this->collectionAttributeSearch($property_name, $identifier));
    }

    /**
     * Checks whether the underlying Collection in the given property has a value
     * with the given identifier.
     *
     * @param string $property_name
     * @param mixed $identifier
     *
     * @return boolean
     */
    protected function hasCollectionAttributeValue($property_name, $identifier)
    {
        return !!$this->getCollectionAttributeValue($property_name, $identifier);
    }

    /**
     * Adds an array or collection of values into the underlying Collection in the given property.
     *
     * @param string $property_name
     * @param mixed $values
     *
     * @return self
     */
    protected function addCollectionAttributeValues($property_name, $new_values)
    {
        foreach($new_values as $new_value) {
            $this->ensureCollectionAttributeIsNotPresent($property_name, $new_value);
        }

        return $this->insertValues($property_name, $this->$property_name->count(), $new_values);
    }

    /**
     * Prepends an array or collection of values onto the underlying Collection in the
     * given property, ensuring that they are in the same order at the start of the
     * collection as they are provided in.
     *
     * @param string $property_name
     * @param mixed $values
     *
     * @return self
     */
    protected function prependCollectionAttributeValues($property_name, $new_values)
    {
        foreach ($new_values as $new_value) {
            $this->ensureCollectionAttributeIsNotPresent($property_name, $new_value);
        }

        return $this->insertValues($property_name, 0, $new_values);
    }

    /**
     * Inserts an array or Collection of new values into the underlying Collection in the
     * given property name before an item with a specified identifier.
     *
     * If no Destination Value can be found, the new values will be appended to the
     * Collection
     *
     * @param string $property_name
     * @param mixed $identifier
     * @param mixed $new_values
     *
     * @return self
     */
    protected function addCollectionAttributeValuesBefore($property_name, $identifier, $new_values)
    {
        foreach($new_values as $new_value) {
            $this->ensureCollectionAttributeIsNotPresent($property_name, $new_value);
        }

        $index = $this->getCollectionAttributeValueCurrentIndex($property_name, $identifier);

        if ($index === false) {
            $index = $this->$property_name->count();
        }

        return $this->insertValues($property_name, $index, $new_values);
    }

    /**
     * Inserts an array or Collection of new values into the underlying Collection in the
     * given property name after an item with a specified identifier.
     *
     * If no Destination Value can be found, the new values will be appended to the
     * Collection
     *
     * @param string $property_name
     * @param mixed $identifier
     * @param mixed $new_values
     *
     * @return self
     */
    protected function addCollectionAttributeValuesAfter($property_name, $identifier, $new_values)
    {
        foreach ($new_values as $new_value) {
            $this->ensureCollectionAttributeIsNotPresent($property_name, $new_value);
        }

        $index = $this->getCollectionAttributeValueCurrentIndex($property_name, $identifier);

        if ($index === false) {
            $index = $this->$property_name->count();
        }

        return $this->insertValues($property_name, $index + 1, $new_values);
    }

    /**
     * Adds an array of Collection of items to the end of the underlying Collection
     * in the given property_name.
     *
     * @param string $property_name
     * @param mixed $new_values
     *
     * @return self
     */
    protected function appendCollectionAttributeValues($property_name, $new_values)
    {
        return $this->addCollectionAttributeValues($property_name, $new_values);
    }

    /**
     * Removes a Collection of values from the underlying Collection in the given
     * property name, returning the removed items.
     *
     * @param string $property_name
     * @param mixed $identifiers
     *
     * @return mixed
     */
    protected function extractCollectionAttributeValues($property_name, $identifiers)
    {
        $removed_items = new Collection;

        foreach($identifiers as $identifier) {
            $index = $this->getCollectionAttributeValueCurrentIndex($property_name, $identifier);

            if ($index !== false) {
                $removed_items->push($this->$property_name->splice($index, 1)->first());
            }
        }

        return $removed_items;
    }

    /**
     * Moves a set of Collection Values from their original places to the position directly before
     * the specified value within the underlying Collection.
     *
     * @param string $property_name
     * @param mixed $destination_identifier
     * @param mixed $identifiers
     *
     * @return self
     */
    protected function moveCollectionAttributeValuesBefore($property_name, $destination_identifier, $identifiers)
    {
        $transfers = $this->extractCollectionAttributeValues($property_name, $identifiers);

        $index = $this->getCollectionAttributeValueCurrentIndex($property_name, $destination_identifier);

        if ($index === false) {
            $index = $this->$property_name->count();
        }

        return $this->insertValues($property_name, $index, $transfers);
    }

    /**
     * Moves a set of Collection Values from their original places to the position directly after
     * the specified value within the underlying Collection.
     *
     * @param string $property_name
     * @param mixed $destination_identifier
     * @param mixed $identifiers
     *
     * @return self
     */
    protected function moveCollectionAttributeValuesAfter($property_name, $destination_identifier, $identifiers)
    {
        $transfers = $this->extractCollectionAttributeValues($property_name, $identifiers);

        $index = $this->getCollectionAttributeValueCurrentIndex($property_name, $destination_identifier);

        if ($index === false) {
            $index = $this->$property_name->count();
        }

        return $this->insertValues($property_name, $index + 1, $transfers);
    }

    /**
     * Removes a set of Collection Values from the underlying Collection
     *
     * @param string $property_name
     * @param mixed $identifiers
     *
     * @return self
     */
    protected function removeCollectionAttributeValues($property_name, $identifiers)
    {
        $this->extractCollectionAttributeValues($property_name, $identifiers);

        return $this;
    }
}