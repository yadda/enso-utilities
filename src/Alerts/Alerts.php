<?php

namespace Yadda\Enso\Utilities\Alerts;

use Illuminate\Support\MessageBag;

class Alerts
{
    protected $alerts;

    public function __construct()
    {
        if (session()->has('alerts')) {
            $alerts = session()->get('alerts');
        } else {
            $alerts = [];
        }

        $this->alerts = new MessageBag($alerts);
    }

    /**
     * Test whether an alert of a given type exists
     *
     * @param  string  $type
     * @return boolean
     */
    public function has($type = null)
    {
        return $this->alerts->has($type);
    }

    /**
     * Add a message to the bag
     *
     * @param string $text
     * @param string $type
     */
    public function add($text, $type = 'default')
    {
        $this->alerts->add($type, $text);
        $this->store();
    }

    /**
     * Shorthand for add method
     *
     * @param  string $text
     * @return void
     */
    public function error($text)
    {
        $this->add($text, 'error');
    }

    /**
     * Shorthand for add method
     *
     * @param  string $text
     * @return void
     */
    public function primary($text)
    {
        $this->add($text, 'primary');
    }

    /**
     * Shorthand for add method
     *
     * @param  string $text
     * @return void
     */
    public function success($text)
    {
        $this->add($text, 'success');
    }

    /**
     * Shorthand for add method
     *
     * @param  string $text
     * @return void
     */
    public function warning($text)
    {
        $this->add($text, 'warning');
    }

    /**
     * Shorthand for add method
     *
     * @param  string $text
     * @return void
     */
    public function info($text)
    {
        $this->add($text, 'info');
    }

    /**
     * Remove all message from the bag and return them.
     *
     * Resulting array will contain a single list of messages, each with their
     * type as an attribute.
     *
     * @param  string $format  Format for the messages
     * @return array
     */
    public function all($format = null) {
        $all = $this->alerts->getMessages($format);
        $alerts = [];

        foreach ($all as $type => $messages) {
            foreach ($messages as $message) {
                $alerts[] = [
                    'type' => $type,
                    'message' => $message,
                ];
            }
        }

        $this->alerts = new MessageBag();
        $this->store();

        return $alerts;
    }

    /**
     * Return all messages from the bag.
     *
     * Returns an array with message types as keys and arrays of message strings
     * as values.
     *
     * @return array
     */
    public function getMessages() {
        $alerts = $this->alerts->getMessages();
        $this->alerts = new MessageBag();
        $this->store();
        return $alerts;
    }

    /**
     * Return all messages of a given type and remove them from the bag
     *
     * Returns an array of message strings
     *
     * @param  string $type
     * @return array
     */
    public function get($type)
    {
        $output = [];

        if (is_null($type)) {
            $output = $this->alerts;
            $this->alerts = null;
        } else if (isset($this->alerts[$type])) {
            $output = $this->alerts[$type];
            unset($this->alerts[$type]);
        }

        $this->store();

        return $output;
    }

    /**
     * Store the message bag in the session
     *
     * @return void
     */
    protected function store()
    {
        if (count($this->alerts) === 0) {
            session()->forget('alerts');
        } else {
            session()->put('alerts', $this->alerts->toArray());
        }
    }

    /**
     * Return the view path for a given alert type
     *
     * Uses namespaced views (allowing the user to override them) and defaults
     * to "default" if $type isn't recognised
     *
     * @param  string $type
     * @return string
     */
    public function getView($type)
    {
        if (view()->exists('enso-alerts::' . $type)) {
            return 'enso-alerts::' . $type;
        } else {
            return 'enso-alerts::default';
        }
    }

    /**
     * Converts alerts to json string
     *
     * @return string
     */
    public function toJson()
    {
        return $this->alerts->toJson();
    }
}
