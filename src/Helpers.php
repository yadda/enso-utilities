<?php

namespace Yadda\Enso\Utilities;

class Helpers
{
    /**
     * Gets just the class name of a Concrete implmentation from the IoC,
     * instead of actually instantiating it
     *
     * @param  string   $abstract   Abstract class to find concrete binding of
     * @return string               Concrete binding
     */
    public static function getConcreteClass($abstract)
    {
        // Get application instance
        $app = \Illuminate\Foundation\Application::getInstance();

        // Get defined bindings
        $bindings = $app->getBindings();

        // Find the binding you want, eg. Kernel
        if (!isset($bindings[$abstract])) {
            // If it doesn't exist, return the original class
            return $abstract;
        }

        $binding = $bindings[$abstract];

        // Get the Closure that produces the concrete class
        $closure = $binding['concrete'];

        // Instantiate a ReflectionFunction
        $closureReflection = new \ReflectionFunction($closure);

        // This is what we pass in by `use` in `function() use ($foo, $bar, $baz) {}`
        $staticVariables = $closureReflection->getStaticVariables();

        // This is the concrete class name, in this case `App\Http\Kernel`
        $concreteClassName = $staticVariables['concrete'];

        return $concreteClassName;
    }
}
