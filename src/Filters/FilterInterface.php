<?php 

namespace Yadda\Enso\Utilities\Filters;

/**
 * Filter apply functionality to data. 
 * 
 * Filters are expected to receive data, modify it, and
 * then return the new data.
 */
interface FilterInterface
{
    /**
     * Fluent accessor function for order that
     * acts as both a getter and setter
     *
     * @param integer|null $order
     * 
     * @return mixed
     */
    function order($order = null);

    /**
     * Gets the current order for this FieldFilter
     *
     * @return integer|float
     */
    function getOrder();

    /**
     * Sets the current order for this FieldFilter
     *
     * @param integer|float $order
     * 
     * @return self
     */
    function setOrder(int $order);

    /**
     * Applies the Filter to the provided data
     *
     * @param mixed $data
     * 
     * @return mixed
     */
    function applyFilter($data);
}