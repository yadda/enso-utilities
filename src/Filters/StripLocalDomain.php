<?php

namespace Yadda\Enso\Utilities\Filters;

use Yadda\Enso\Utilities\DomainNames\DomainNames;
use Yadda\Enso\Utilities\DomainNames\DomainNamesWorker;
use Yadda\Enso\Utilities\Filters\Filter;
use Yadda\Enso\Utilities\Filters\FilterException;
use Yadda\Enso\Utilities\Filters\FilterInterface;

/**
 * Filter that converts links to 
 */
class StripLocalDomain extends Filter implements FilterInterface
{
    /**
     * Applies the Filter to the provided data
     *
     * @param string $data
     * 
     * @return string
     */
    public function applyFilter($data)
    {
        return preg_replace($this->getRegex(), '/', $data);
    }

    /**
     * Gets the regex required to apply this filter
     *
     * @return string
     */
    protected function getRegex()
    {
        if (!app()->bound(DomainNamesWorker::class)) {
            throw new FilterException('DomainNames worker and/or facade is not registered');
        }

        return '#(http(s)?:\/\/)?([a-zA-Z0-9-]+\.){0,}(?<![-a-zA-Z0-9])' . DomainNames::domain() . '\/?(?!\.)#';
    }
}