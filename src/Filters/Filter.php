<?php 

namespace Yadda\Enso\Utilities\Filters;

abstract class Filter
{
    protected $order;

    /**
     * Fluent accessor function for order that
     * acts as both a getter and setter
     *
     * @param integer|null $order
     * 
     * @return mixed
     */
    public function order($order = null)
    {
        if (is_null($order)) {
            $this->getOrder();
        }

        return $this->setOrder($order);
    }

    /**
     * Gets the current order for this FieldFilter
     *
     * @return integer|float
     */
    public function getOrder()
    {
        return $this->order ?? 100;
    }

    /**
     * Sets the current order for this FieldFilter
     *
     * @param integer|float $order
     * 
     * @return self
     */
    public function setOrder(int $order)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Applies the Filter to the provided data
     *
     * @param mixed $data
     * 
     * @return mixed
     */
    abstract public function applyFilter($data);
}