<?php 

namespace Yadda\Enso\Utilities\DomainNames;

class DomainNamesWorker
{
    /**
     * Allowed Domains to use.
     *
     * @var array
     */
    protected $domains;

    /**
     * Allowed subdomains to use
     *
     * @var array
     */
    protected $subdomains;

    /**
     * Current domain
     *
     * @var string
     */
    protected $current_domain;
    
    /**
     * Current subdomain
     *
     * @var string
     */
    protected $current_subdomain;

    /**
     * Calculates the initial state of this singleton. Expects an array of 
     * allowed subdomains and an array of allowed domains.
     * 
     * Not specifying allowed subdomains will mean any are allowed.
     * 
     * Not specifying allowed domains will default to the app's url from 
     * the config.
     *
     * @param array $subdomains
     * @param array $domains
     */
    public function __construct($subdomains = [], $domains = [])
    {
        if (empty($domains)) {
            // Remove protocol from app url and use that
            $this->domains = [preg_replace('#(http(s)?:?)(//)?(www.)?#', '', config('app.url'))];
        } else {
            $this->domains = $domains;
        }

        $this->subdomains = $subdomains;
        
        $host = request()->server('HTTP_HOST');
        
        foreach($this->domains as $domain) {
            if (strpos($host, $domain) !== false) {
                $this->current_domain = $domain;

                $subdomain = trim(preg_replace('#\.?' . $domain . '#', '', $host), ' .');

                if (
                    !empty($subdomain)
                    && (
                        empty($this->subdomains)
                        || in_array($subdomain, $this->subdomains)
                    )
                ) {
                    $this->current_subdomain = $subdomain;
                }

                break;
            }
        }

        // If we still can't match just the domain, just use the full host string as the domain
        if (is_null($this->current_domain)) {
            $this->current_domain = $host;
        }
    }

    /**
     * Gets the current domain
     *
     * @return string|null
     */
    public function domain()
    {
        return $this->current_domain;
    }

    /**
     * Checks whether the current domain matches the given value.
     * 
     * Optionally enable a loose version, which only uses a loose 
     * comparator and is case insensitive.
     *
     * @param string $domain
     * @param boolean $strict
     * 
     * @return boolean
     */
    public function domainIs(string $domain, $strict = true)
    {
        if ($strict) {
            return $this->current_domain === $domain;
        } else {
            return strtolower($this->current_domain) == strtolower($domain);
        }
    }

    /**
     * Gets the current subdomain
     *
     * @return string|null
     */
    public function subdomain()
    {
        return $this->current_subdomain;
    }

    /**
     * Checks whether the current subdomain matched the given value.
     * 
     * Optionally enable a loose version, which only uses a loose 
     * comparator and is case insensitive.
     *
     * @param string $subdomain
     * @param boolean $strict
     * 
     * @return boolean
     */
    public function subdomainIs(string $subdomain, $strict = true)
    {
        if ($strict) {
            return $this->current_subdomain === $subdomain;
        } else {
            return strtolower($this->current_subdomain) == strtolower($subdomain);
        }
    }

    /**
     * Tests to see if there is no subdomain set.
     *
     * @return boolean
     */
    public function subdomainEmpty()
    {
        return is_null($this->current_subdomain);
    }

    /**
     * Tests to see if there is a subdomain set.
     *
     * @return boolean
     */
    public function subdomainNotEmpty()
    {
        return !is_null($this->current_subdomain);
    }
}