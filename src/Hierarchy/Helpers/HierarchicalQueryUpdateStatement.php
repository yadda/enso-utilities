<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers;

use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchicalQueryUpdateColumn;

/**
 * Allows for the building of simple update statements on a given table using
 * CASE - WHEN - THEN conditions.
 *
 * As a note: currently, adding multiple WHERE statements only use either 'AND'
 * or 'OR' conditions, not a mixture of the two. This meets the needs of the
 * class at present, so adding more complicated functionality is not a good use
 * of time at present. This may get revisited later.
 *
 * This could also do with some checking and added functionality around adding
 * another column with the same name, but again is beyond the current scope of
 * the project.
 */
class HierarchicalQueryUpdateStatement {

    protected $id_column;
    protected $table_name;
    protected $old_table_name;

    // Columns to update;
    protected $columns = [];

    // Where clause(s)
    protected $where = [];
    protected $where_type = "OR";

    protected $generated_data;

    public function __construct($table_name, $old_table_name, $id_column)
    {
        $this->id_column = $id_column;
        $this->table_name = $table_name;
        $this->old_table_name = $old_table_name;
    }

    /**
     * Add a column to the statement
     *
     * @param  HierarchicalQueryUpdateColumn $column    [description]
     * @return self
     */
    public function addColumn(HierarchicalQueryUpdateColumn $column)
    {
        $column->setTableName($this->table_name);
        $column->setOldTableName($this->old_table_name);

        $this->columns[] = $column;

        return $this;
    }

    /**
     * Adds a where clause for this statement
     *
     * @param  string       $clause                     where clause
     * @return self
     */
    public function where($clause) {
        $this->where[] = $clause;

        return $this;
    }

    /**
     * Sets the joining condition for where clauses
     *
     * @param string        $type                       where join type
     * @return self
     */
    public function setWhereType($type)
    {
        if (!in_array($type, ['OR', 'AND'])) {
            throw new Exception('Invalid where concatenation type provided');
        }

        $this->where_type = $type;

        return $this;
    }

    /**
     * Gets the full MySQL statement for this object
     *
     * @return string                                   MySQL statement
     */
    public function getSql()
    {
        // Set the update clause, and inner join it on itself. This is a
        // requirement for MySQL, as it updates columns in series (as opposed to
        // sqlite which does it in parallel), so it cannot otherwise update the
        // left_id column, and then update theh right_id column based on the
        // value that left_id USED to hold before it was updated.
        $string = "UPDATE {$this->table_name} INNER JOIN {$this->table_name} AS {$this->old_table_name} ON {$this->table_name}.{$this->id_column} = {$this->old_table_name}.{$this->id_column} SET ";

        $columns = [];
        foreach($this->columns as $column) {
            $columns[] = $column->getSql(
                $this->table_name,
                $this->old_table_name
            );
        }

        $string .= implode(', ', $columns);

        return $string . $this->makeWhereClause();
    }

    /**
     * Makes a WHERE clause based on the $where array on this object, if not
     * empty
     *
     * @return string                                   WHERE clause.
     */
    protected function makeWhereClause()
    {
        if(count($this->where)) {
            return " WHERE " . implode(" {$this->where_type} ", $this->where);
        }

        return null;
    }
}
