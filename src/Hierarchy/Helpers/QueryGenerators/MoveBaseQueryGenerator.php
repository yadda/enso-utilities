<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators;

use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeInvalidMove;
use Carbon\Carbon;
use Exception;

use Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators\BaseQueryGenerator;
use Yadda\Enso\Utilities\Hierarchy\Helpers\HierarchicalQueryUpdateColumn;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;

abstract class MoveBaseQueryGenerator extends BaseQueryGenerator {

    protected $destination_node;

    public function __construct(IsHierarchical $source_node, IsHierarchical $destination_node)
    {
        parent::__construct($source_node);

        $this->setDestinationNode($destination_node);
    }

    /**
     * Sets the destination_node property after checking that the given node is
     * of the correct class
     *
     * @param IsHierarchical    $node           node to use as destination node
     */
    protected function setDestinationNode(IsHierarchical $destination_node)
    {
        if (!$destination_node instanceof $this->node_class) {
            throw new HierarchicalNodeInvalidMove('Source and Destiation nodes must be instances of the same class');
        }

        $this->moveValidityCheck($destination_node);

        $this->destination_node = $destination_node;
    }

    /**
    * Test to see whether a given node is a current descendant of the currently
    * set source node. This can be used to invalidate any move request, as it
    * shouldn't be possible to move a node to a node that it is a parent of
    * given the current specs
    *
    * @param  IsHierarchical   $potential_child         Potential descendant
    * @param  IsHierarchical   $potential_parent        Potential parent
    * @return boolean                                   Whether is descendant
    */
    protected function nodeIsDescendantOfNode($potential_child, $potential_parent)
    {
        return (
            $potential_parent->getHierarchyLeftId() < $potential_child->getHierarchyLeftId() &&
            $potential_parent->getHierarchyRightId() > $potential_child->getHierarchyRightId()
        );
    }

    /**
     * Determines whether the nodes that are being modified are siblings
     *
     * @return boolean                          whether nodes are siblings
     */
    protected function nodesAreSiblings()
    {
        return ((int) $this->source_node->getHierarchyParentId() === (int) $this->destination_node->getHierarchyParentId());
    }

    /**
     * Gets the minimum value from the source and destination left ids,
     * optionally passing in a modifier to change this value.
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function minLeft($modifier = 0)
    {
        return min(
            $this->source_node->getHierarchyLeftId(),
            $this->destination_node->getHierarchyLeftId()
        ) + $modifier;
    }

    /**
     * Gets the maximum value from the source and destination left ids,
     * optionally passing in a modifier to change this value.
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function maxLeft($modifier = 0)
    {
        return max(
            $this->source_node->getHierarchyLeftId(),
            $this->destination_node->getHierarchyLeftId()
        ) + $modifier;
    }

    /**
     * Gets the minimum value from the source and destination right ids,
     * optionally passing in a modifier to change this value.
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function minRight($modifier = 0)
    {
        return min(
            $this->source_node->getHierarchyRightId(),
            $this->destination_node->getHierarchyRightId()
        ) + $modifier;
    }

    /**
     * Gets the maximum value from the source and destination right ids,
     * optionally passing in a modifier to change this value.
     *
     * @param  integer      $modifier           ammount to modify value by
     * @return integer
     */
    protected function maxRight($modifier = 0)
    {
        return max(
            $this->source_node->getHierarchyRightId(),
            $this->destination_node->getHierarchyRightId()
        ) + $modifier;
    }

    /**
     * Determines whether a move to the given node is a valid move case.
     *
     * @param  IsHierarchical $destination_node [description]
     * @return [type]                           [description]
     */
    abstract protected function moveValidityCheck(IsHierarchical $destination_node);

    /**
    * Determines whether this move is left or right through the tree
    *
    * @return boolean                          true for left, false for right
    */
    abstract protected function moveDirectionIsLeft();

    /**
     * Sets parameters on the builder based on the source and destination nodes,
     * accounting for a move leftwards across the tree.
     *
     * @return void
     */
    abstract protected function applyLeftDirectionSettings();

     /**
     * Sets parameters on the builder based on the source and destination nodes,
     * accounting for a move rigthwards across the tree.
     *
     * @return void
     */
    abstract protected function applyRightDirectionSettings();

    /**
     * Generates a MySQL query string describing the moving on a node (and it's
     * children, if present) to a position relation to another node, base on
     * the values set by previous function calls.
     *
     * @return string                           MySQL query
     */
    public function generateStatement()
    {
        // dd($this->moveDirectionIsLeft());

        if($this->moveDirectionIsLeft()) {
            $this->applyLeftDirectionSettings();
        } else {
            $this->applyRightDirectionSettings();
        }

        // dd($this->modifiers);
        // dd($this->moveDirectionIsLeft(), $this->modifiers, $this->cases, $this->where_clause);

        // Need to manually set the updated_at, as this isn't done through
        // Eloquent
        $now = Carbon::now();

        $this->statement
            ->addColumn((new HierarchicalQueryUpdateColumn($this->left_column_name))
                ->addModifyClause($this->cases['moved_case'], $this->modifiers['moved'])
                ->addModifyClause($this->cases['nodes_between_case'], $this->modifiers['changed'])
                ->addModifyClause($this->cases['update_left_case'], $this->modifiers['changed']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->right_column_name))
                ->addModifyClause($this->cases['moved_case'], $this->modifiers['moved'])
                ->addModifyClause($this->cases['nodes_between_case'], $this->modifiers['changed'])
                ->addModifyClause($this->cases['update_right_case'], $this->modifiers['changed']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->parent_column_name))
                ->addSetClause($this->cases['source_node_case'], $this->new_values['parent_id']))
            ->addColumn((new HierarchicalQueryUpdateColumn($this->depth_column_name))
                ->addModifyClause($this->cases['moved_case'], "+ {$this->modifiers['depth']}"))
            ->addColumn((new HierarchicalQueryUpdateColumn("updated_at"))
                ->setValue("'{$now->__toString()}'"))
            ->where($this->where_clause);

        return $this->statement->getSql();
    }
}
