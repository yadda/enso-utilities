<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators;

use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierachicalQueryNoMoveRequiredException;
use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeInvalidMove;

use Yadda\Enso\Utilities\Hierarchy\Helpers\QueryGenerators\MoveBaseQueryGenerator;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;

class MoveUnderQueryGenerator extends MoveBaseQueryGenerator {

    /**
     * Determines whether a move to the given node is a valid move case.
     *
     * @param  IsHierarchical   $destination_node   Destination node to check
     *                                              move validity for
     * @return void
     */
    protected function moveValidityCheck(IsHierarchical $destination_node)
    {
        if($this->source_node->getKey() === $destination_node->getKey()) {
            throw new HierarchicalNodeInvalidMove('Cannot move a node underneath itself');
        }

        if($this->nodeIsDescendantOfNode($destination_node, $this->source_node)) {
            throw new HierarchicalNodeInvalidMove('Cannot move a node underneath a descendant of itself');
        }
    }

    /**
    * Determines whether this move is left or right through the tree
    *
    * @return boolean                          true for left, false for right
    */
    protected function moveDirectionIsLeft()
    {
        return ((int) $this->destination_node->getHierarchyRightId() < (int) $this->source_node->getHierarchyRightId());
    }

    /**
     * Sets parameters on the build that are common to both leftward and
     * rightward node moves across the tree.
     *
     * @return void
     */
    protected function applyCommonSettings()
    {
        $this->cases['source_node_case'] = $this->getSourcedNodeCase();
        $this->cases['moved_case'] = $this->getSourceNodeAndDescendantsCase();

        // Modifier that should be applied to left and/or right ids of rows that
        // were not targets but are affected by the move (eg, nodes between the
        // source and destination nodes)
        $this->modifiers['unqualified_changed'] = ($this->source_node->getHierarchyRightId() - $this->source_node->getHierarchyLeftId()) + 1;

        // New parent_id of source_node should be id of destination_node.
        $this->new_values['parent_id'] = $this->destination_node->getKey();

        // Modifier should be the depth of the destination node minus the depth
        // of the source node. Then add 1 to the modifier because we're making
        // this a child of the destination node.
        $difference = $this->destination_node->getHierarchyDepth() - $this->source_node->getHierarchyDepth();
        $this->modifiers['depth'] = ($difference + 1);

        // Nodes between case is all nodes whose original left + right ids fall
        // between the low_node
        $this->cases['nodes_between_case'] = $this->getNodesBetweenCase();

        // Generates a where clause to exclude nodes that aren't changed at all
        $this->where_clause = $this->getWhereClause();
    }

    /**
     * Sets parameters on the builder based on the source and destination nodes,
     * accounting for a move leftwards across the tree.
     *
     * @return void
     */
    protected function applyLeftDirectionSettings()
    {
        $this->applyCommonSettings();

        // Find the difference between the destination node right id and the
        // source left id. This will make the new left_id equal to the old
        // destination right_id (which is true for putting a node under a node
        // as the new last child)
        $modifier = abs($this->destination_node->getHierarchyRightId() - $this->source_node->getHierarchyLeftId());

        // If node will end up in the same position, throw exception
        if($modifier === 0) {
            throw new HierachicalQueryNoMoveRequiredException();
        }

        // Moving left means moved items should have a negative modifier
        $this->modifiers['moved'] = "- {$modifier}";

        // Moving left means changed items should have a positive modifier
        $this->modifiers['changed'] = "+ {$this->modifiers['unqualified_changed']}";

        // Select items whose left ids need updating (e.g. parents of the high node);
        $this->cases['update_left_case'] = $this->getAscendantsCase($this->source_node);

        // Select items whose right ids need updating (e.g. parents of the low node);
        $this->cases['update_right_case'] = $this->getAscendantsCase($this->destination_node);
    }

    /**
     * Sets parameters on the builder based on the source and destination nodes,
     * accounting for a move rigthwards across the tree.
     *
     * @return void
     */
    protected function applyRightDirectionSettings()
    {
        $this->applyCommonSettings();

        // Find the difference between the destination node right id and the
        // source left id, then subtract the changed_ids modifier to account for
        // the tree needing to shift across to fill the gap left by moved the
        // nodes.
        $difference = $this->destination_node->getHierarchyRightId() - $this->source_node->getHierarchyLeftId();
        $modifier = (($difference - $this->modifiers['unqualified_changed']));

        // If node will end up in the same position, throw exception
        if($modifier === 0) {
            throw new HierachicalQueryNoMoveRequiredException();
        }

        // Moving right means moved items should have a positive modifier
        $this->modifiers['moved'] = "+ {$modifier}";

        // Moving right means changed items should have a negative modifier
        $this->modifiers['changed'] = "- {$this->modifiers['unqualified_changed']}";

        // Select items whose left ids need updating (e.g. parents of the high node);
        $this->cases['update_left_case'] = $this->getAscendantsCase($this->destination_node);

        // Select items whose right ids need updating (e.g. parents of the low node);
        $this->cases['update_right_case'] = $this->getAscendantsCase($this->source_node);
    }

    /**
     * Generates a where clause to exclude all nodes which are ourside of the
     * bounds of the query, to reduce the number of rows to run a query over
     * that would otherwise set 'value' = 'value' for every column.
     *
     * @return string                           generated where clause
     */
    protected function getWhereClause()
    {
        $modifier = 0;

        // A modifier is added when the source is a descendant of the
        // destination so because under those circumstanes, the destination will
        // remain unchanged, and could otherwise get caught by the
        // 'update_left_case' or 'update_right_case' clauses.
        if ($this->nodeIsDescendantOfNode($this->source_node, $this->destination_node)) {
            $modifier = 1;
        }

        return
            "(".
                "{$this->old_table_name}.{$this->left_column_name} BETWEEN {$this->minLeft($modifier)} AND {$this->maxRight(-$modifier)}".
                " OR ".
                "{$this->old_table_name}.{$this->right_column_name} BETWEEN {$this->minLeft($modifier)} AND {$this->maxRight(-$modifier)}".
            ")";
    }

    /**
     * Prepares a MySQL CASE clause for all rows that are wholly between the
     * source and destination nodes.
     *
     * @return string                               case that describes the nodes
     *                                              wholly between the two
     */
    protected function getNodesBetweenCase()
    {
        $high_value = $this->maxRight();
        $low_value = $this->minLeft();

        // If the source node is a descendant of the destination node, then we
        // need to use the source_node's left id. Using the destination node's
        // left id would then include ALL descendants of the destination node,
        // not just those that are changed.
        if ($this->nodeIsDescendantOfNode($this->source_node, $this->destination_node)) {
            $low_value = $this->maxLeft();
        }

        return
            "(".
                "{$this->old_table_name}.{$this->left_column_name} > {$low_value}".
                " AND ".
                "{$this->old_table_name}.{$this->right_column_name} < {$high_value}".
            ")";
    }
}
