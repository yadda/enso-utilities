<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers;

use Illuminate\Support\Collection;

class HierarchyTree extends Collection {

    protected $nodes;

    public function __construct(Collection $nodes)
    {
        $this->nodes = $nodes;

        $this->buildTree();
    }

    protected function buildTree()
    {
        if($this->nodes->count() === 0) {
            return $this;
        }

        do {
            $node = $this->nodes->shift();

            if ($node->isRootNode()) {
                $this->items[] = $node;
            } else {
                $this->items[$this->count() - 1]->addTreeElement($node);
            }

        } while ($this->nodes->count());

        return $this;
    }

    protected function findParentInTree($node)
    {

    }
}
