<?php

namespace Yadda\Enso\Utilities\Hierarchy\Helpers;

use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeInvalidUpdateColumn;

/**
 * Helper for building Hierarchy Tree queries in a more readable fashion
 */
class HierarchicalQueryUpdateColumn {

    protected $column_name;
    protected $table_name;
    protected $old_table_name;

    protected $value;

    protected $modify_cases = [];
    protected $modify_values = [];

    protected $set_cases = [];
    protected $set_values = [];

    /**
     * Must sepcific the column being updated
     *
     * @param string        $column_name    column to run update on
     */
    public function __construct($column_name)
    {
        $this->column_name = $column_name;
    }

    /**
    * Sets the table_name property. This is a workaround to make MySQL
    * use an INNER JOIN to keep track of the 'old' values, as it runs an
    * UPDATE query on columns in series, not parallel.
    *
    * @param string        $table_name     name of table
    */
    public function setTableName($table_name)
    {
        $this->table_name = $table_name;
    }

    /**
     * Sets the old_table_name property. This is a workaround to make MySQL
     * use an INNER JOIN to keep track of the 'old' values, as it runs an
     * UPDATE query on columns in series, not parallel.
     *
     * @param string        $old_table_name     name of old table
     */
    public function setOldTableName($old_table_name)
    {
        $this->old_table_name = $old_table_name;
    }

    /**
     * Sets the value property, for setting items which will not use a case
     * statement
     *
     * @param  string       $value          value to set
     * @return self
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Allows adding a clause to set the column to a given value
     *
     * @param string        $case           case when should be set
     * @param string        $set            value to set
     */
    public function addSetClause($case, $set)
    {
        $this->set_cases[] = $case;
        $this->set_values[] = $set;

        return $this;
    }

    /**
     * Allows adding a clause to modify the column by a given value. The
     * modifier is expected in the format or "$operation $value", so "+ 1" or
     * "- 12".
     *
     * @param string        $case           case when should be set
     * @param string        $modify         modifier to use.
     */
    public function addModifyClause($case, $modify)
    {
        $this->modify_cases[] = $case;
        $this->modify_values[] = $modify;

        return $this;
    }

    /**
     * Gets the string that this UpdateColumn statment generates
     *
     * @return string                       MySQL column update statement
     */
    public function getSql()
    {
        $this->validityCheck();

        // Return just value if set
        if (!is_null($this->value)) {
            return "{$this->table_name}.{$this->column_name} = {$this->value}";
        }

        // Otherwise, build with CASE - WHEN - THEN conditions;
        $string = "{$this->table_name}.{$this->column_name} = (CASE ";

        // Apply each clause
        foreach ($this->modify_cases as $index => $case) {
            $string .= "WHEN {$case} THEN ({$this->old_table_name}.{$this->column_name} {$this->modify_values[$index]}) ";
        }

        foreach ($this->set_cases as $index => $case) {
            $string .= "WHEN {$case} THEN ({$this->set_values[$index]}) ";
        }

        // Ends the statement with an 'Else, leave it as it is' condition.
        return $string .= "ELSE {$this->old_table_name}.{$this->column_name} END)";
    }

    /**
     * Throws an exception if the Update column statement isn't valid when
     * called.
     *
     * @return void
     */
    protected function validityCheck()
    {
        if(empty($this->table_name)) {
            throw new HierarchicalNodeInvalidUpdateColumn('Table Name not set');
        }

        if(empty($this->old_table_name)) {
            throw new HierarchicalNodeInvalidUpdateColumn('Old table Name not set');
        }

        if(empty($this->value) && empty($this->modify_cases) && empty($this->set_cases)) {
            throw new HierarchicalNodeInvalidUpdateColumn('No Values or Cases to set');
        }

        return;
    }
}
