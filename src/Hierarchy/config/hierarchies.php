<?php

return [

    'default_repository_class' => Yadda\Enso\Utilities\Hierarchy\Repositories\HierarchyRepository::class,
    'custom_repositories' => [

        // Yadda\Enso\Utilities\Models\Role::class => Yadda\Enso\Utilities\Hierarchy\Repositories\HierarchyRepository::class

    ]

];
