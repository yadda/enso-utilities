<?php

namespace Yadda\Enso\Utilities\Hierarchy\Contracts;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical;

interface HierarchicalRepository {

    /**
     * Gets a list of all Nodes that are higher up (but still related) in the
     * current tree node.
     *
     * @return Collection                   Ascendant nodes
     */
    function ascendants();

    /**
     * Returns the current_node if it is a root node. Otherwise, returns the
     * first element of the ascendantNodes collection.
     *
     * @return IsHierarchical               Root Node, or self
     */
    function root();

    /**
     * Grabs the last element on the ascendantNodes collection to return as the
     * parent of the current node. If his is a root node, just returns null
     *
     * @return IsHierarchical|null          Parent Node, of null if root.
     */
    function parent();

    /**
     * Returns a collection of all of the siblings of the current_node
     *
     * @return Collection                   Siblings
     */
    function siblings();

    /**
     * Returns a collection of all of the descendants of the current_node
     *
     * @return Collection                           Descendants
     */
    function descendants();

    /**
     * Filters the descendants list (to avoid requerying the same data) to
     * provide a list of direct children of the current_node
     *
     * @return Collection                           Children
     */
    function children();

    /**
     * Creates a new node of the class passed in. This will initially set it up
     * as a new root node.
     *
     * @param  string           $class              Class of node to create
     * @param  array            $attributes         Attributes to set
     * @return IsHierarchical                       created node
     */
    static function create($class, $attributes = []);

    /**
     * Deletes a given node, and if specified, it's children.
     *
     * @param  IsHierarchical   $node               Node to delete
     * @param  boolean          $delete_children    Whether to delete branch or
     *                                              node
     * @return boolean                              Success status
     */
    static function delete(IsHierarchical $node, $delete_children = false);

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $parent_node    Node that will house the moved node
     * @return boolean                          Success status
     */
    function moveNodeUnderNode(IsHierarchical $parent_node);

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $parent_node    Node that will house the moved node
     * @return boolean                          Success status
     */
    function moveNodeAfterNode(IsHierarchical $parent_node);

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @param  IsHierarchical   $parent_node    Node that will house the moved node
     * @return boolean                          Success status
     */
    function moveNodeBeforeNode(IsHierarchical $parent_node);

    /**
     * Defers call to dedicated Hierarchical Query helper
     *
     * @return boolean                          Success status
     */
    function convertToRootNode();

    /**
     * Deletes the current node. Throws an exception if the current node has any
     * descendants
     *
     * @return boolean                          Success status
     */
    function deleteNode();

    /**
     * Deletes the current node and all it's children
     *
     * @return Boolean                          Success status
     */
    function deleteBranch();
}
