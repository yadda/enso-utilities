<?php

namespace Yadda\Enso\Utilities\Hierarchy\Traits;

/**
 *
 */
trait IsTreeElement {

    protected $hierarchy_tree_children = [];

    public function hasChildren()
    {
        return count($this->hierarchy_tree_children);
    }

    public function getChildren()
    {
        return $this->hierarchy_tree_children;
    }

    public function addTreeElement($node) {
        if ($node->isChildOf($this)) {
            $this->hierarchy_tree_children[] = $node;
        } elseif ($node->isDescendantOf($this)) {
            $this->insertAsSubChild($node);
        }

        return;
    }

    protected function insertAsSubChild(self $node)
    {
        foreach($this->hierarchy_tree_children as $index => $child) {
            if($node->isChildOf($child)) {
                $child->hierarchy_tree_children[] = $node;
                return;
            } elseif ($node->isDescendantOf($child)) {
                $child->insertAsSubChild($node);
                return;
            }
        }

        return;
    }
}
