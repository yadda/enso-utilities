<?php

namespace Yadda\Enso\Utilities\Hierarchy\Traits;

use Yadda\Enso\Utilities\Hierarchy\Contracts\IsHierarchical as IsHierarchicalContract;
use Yadda\Enso\Utilities\Hierarchy\Traits\IsTreeElement;

/**
 * Trait that can be applied to Eloquent models to add functionality based
 * around storing them in a left/right hierarchical tree.
 */
trait IsHierarchical {

    use IsTreeElement;

    protected $hierarchy_update_object;

    /**
     * Gets name of parent_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   parent_id column name
     */
    public function getParentIdColumnName()
    {
        return (property_exists($this, 'hierarchical_parent_id_column_name')) ?
            $this->hierarchical_left_id_column_name :
            'parent_id';
    }

    /**
     * Gets the parent id of the current node
     *
     * @return integer                  parent_id or null (if root node)
     */
    public function getHierarchyParentId()
    {
        return $this->attributes[$this->getParentIdColumnName()];
    }

    /**
     * Set the parent id of the current node
     *
     * @param void
     */
    public function setHierarchyParentId($value)
    {
        $this->{$this->getParentIdColumnName()} = $value;
    }

    /**
     * Gets name of left_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   left_id column name
     */
    public function getLeftIdColumnName()
    {
        return (property_exists($this, 'hierarchical_left_id_column_name')) ?
            $this->hierarchical_left_id_column_name :
            'left_id';
    }

    /**
     * Gets the left id of the current node
     *
     * @return integer                  left_id
     */
    public function getHierarchyLeftId() {
        return $this->attributes[$this->getLeftIdColumnName()];
    }

    /**
    * Set the parent id of the current node
    *
    * @param void
    */
    public function setHierarchyLeftId($value)
    {
        $this->{$this->getLeftIdColumnName()} = $value;
    }

    /**
     * Gets name of right_id column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   right_id column name
     */
    public function getRightIdColumnName()
    {
        return (property_exists($this, 'hierarchical_right_id_column_name')) ?
            $this->hierarchical_right_id_column_name :
            'right_id';
    }

    /**
     * Gets the right id of the current node
     *
     * @return integer                  right_id
     */
    public function getHierarchyRightId() {
        return $this->attributes[$this->getRightIdColumnName()];
    }

    /**
    * Set the parent id of the current node
    *
    * @param void
    */
    public function setHierarchyRightId($value)
    {
        $this->{$this->getRightIdColumnName()} = $value;
    }

    /**
     * Gets name of depth column. Allows for overriding if other column name
     * desired.
     *
     * @return string                   depth column name
     */
    public function getDepthColumnName()
    {
        return (property_exists($this, 'hierarchical_depth_column_name')) ?
            $this->hierarchical_depth_column_name :
            'depth';
    }

    /**
     * Gets the depth id of the current node
     *
     * @return integer                  depth
     */
    public function getHierarchyDepth() {
        return $this->attributes[$this->getDepthColumnName()];
    }

    /**
    * Set the parent id of the current node
    *
    * @param void
    */
    public function setHierarchyDepth($value)
    {
        $this->{$this->getDepthColumnName()} = $value;
    }

    /**
     * Gets an instance of the HierarchyRepository for moving this item
     * within it's hierarchy tree.
     *
     * @param  bolearn              $reload     Force Hierarchy reload.
     * @return HierarchyRepository
     */
    public function getHierarchyRepository($reload = false)
    {
        if (is_null($this->hierarchy_update_object) || $reload) {

            if (is_null($repostiory_class = config('enso-utilities.hierarchies.custom_repositories.' . get_class($this)))) {
                $repostiory_class = config('enso-utilities.hierarchies.default_repository_class');
            }

            $this->hierarchy_update_object = new $repostiory_class($this);
        }

        return $this->hierarchy_update_object;
    }

    /**
     * Boolean check to see whether this node is a root node
     *
     * @return boolean
     */
    public function isRootNode()
    {
        $parent_id_name = $this->getParentIdColumnName();
        return is_null($this->$parent_id_name);
    }

    /**
     * Test to see if this is the child of a given node
     *
     * @param  IsHierarchical   $node       Node to test
     * @return boolean                      Whether is child
     */
    public function isChildOf($node) {
        return (int) $node->getKey() === (int) $this->getHierarchyParentId();
    }

    /**
     * Tests a node to see whether this is a descendant of it
     *
     * @param  IsHierarchical   $node       Node to test
     * @return boolean                      Whether is descendant
     */
    public function isDescendantOf($node)
    {
        return (
            $this->getHierarchyLeftId() > $node->getHierarchyLeftId() &&
            $this->getHierarchyRightId() < $node->getHierarchyRightId()
        );
    }

    /**
     * Gets the root node of this node, or self if it IS the root node
     *
     * @return IsHierarchical           Root Node
     */
    public function rootNode()
    {
        return $this->getHierarchyRepository()->root();
    }

    /**
     * Gets the parent node of this node, or null if this is a root node.
     *
     * @return IsHierarchical           Parent node
     */
    public function parentNode()
    {
        return $this->getHierarchyRepository()->parent();
    }

    /**
     * Boolean check to see whether this node has any siblings, or optionally
     * whether it has a specific given number of siblings
     *
     * @param  integer      $number     Specific number of siblings to check for
     * @return boolean
     */
    public function hasSiblingNodes($number = null) {
        return (is_null($number)) ?
            !! $this->siblingNodes()->count() :
            $this->siblingNodes()->count() === (int) $number;
    }

    /**
     * Gets the siblings nodes of this node
     *
     * @return Collection           Sibling nodes
     */
    public function siblingNodes()
    {
        return $this->getHierarchyRepository()->siblings();
    }

    /**
     * Boolean check to see whether this node has any direct children, and
     * optionally whether it has a specific number of children
     *
     * @param  integer      $number     Specific number of siblings to check for
     * @return boolean
     */
    public function hasChildNodes($number = null)
    {
        return (is_null($number)) ?
            !! $this->childNodes()->count() :
            $this->childNodes()->count() === (int) $number;
    }

    /**
     * Gets the direct children of this node
     *
     * @return Collection           Children nodes
     */
    public function childNodes()
    {
        return $this->getHierarchyRepository()->children();
    }

    /**
     * Boolean check to see wether this node has any descendants. Optionally,
     * You can pass in a depth to limit the check to only descendants at least
     * $depth levels deep. (direct children = 1, grand children = 2 etc...)
     *
     * @param  integer      $min_depth      minimum depth to be counted
     * @return boolean
     */
    public function hasDescendantNodes($min_depth = 1)
    {
        return !! $this->descendantNodes()->filter(function($node) use ($min_depth) {
            return ((int) $node->getHierarchyDepth() >= (int) ($this->getHierarchyDepth() + $min_depth));
        })->count();
    }

    /**
     * Gets all the ascendant nodes of this node
     *
     * @return Collection           Descendant nodes
     */
    public function ascendantNodes()
    {
        return $this->getHierarchyRepository()->ascendants();
    }

    /**
     * Gets all the descendant nodes of this node
     *
     * @return Collection           Descendant nodes
     */
    public function descendantNodes()
    {
        return $this->getHierarchyRepository()->descendants();
    }

    /**
     * Puts this node under a given node, as the new last-child of the passed
     * node
     *
     * @param  self         $node       Node to put this under
     * @return boolean                  Success status
     */
    public function putUnder(self $node)
    {
        return $this->getHierarchyRepository()->moveNodeUnderNode($node);
    }

    /**
     * Puts this node directly after a given node in the tree structure
     *
     * @param  self         $node       Node to put this after
     * @return boolean                  Success status
     */
    public function putAfter(self $node)
    {
        return $this->getHierarchyRepository()->moveNodeAfterNode($node);
    }

    /**
    * Puts this node directly before a given node in the tree structure
    *
    * @param  self         $node       Node to put this before
    * @return boolean                  Success status
    */
    public function putBefore(self $node)
    {
        return $this->getHierarchyRepository()->moveNodeBeforeNode($node);
    }

    /**
     * Deletes this node, via the Repository method. This ensures the user can
     * still modify the eloquent delete() function.
     *
     * @return boolean                  Success status
     */
    public function deleteNode()
    {
        return $this->getHierarchyRepository()->deleteNode();
    }

    /**
     * Deletes this node and all of it's children, via the repository method.
     *
     * @return boolean                  Success status
     */
    public function deleteBranch()
    {
        return $this->getHierarchyRepository()->deleteBranch();
    }

    /**
     * Cuts this node out of the tree and puts it as the new last root element
     * of the tree. If this is already a rootElemenet, do nothing.
     *
     * @return boolean                  Success status
     */
    public function convertToRootNode()
    {
        if ($this->isRootNode()) return true;

        return $this->getHierarchyRepository()->convertToRootNode();
    }

    /**
     * Returns only nodes from the query that are root nodes (i.e. nodes where
     * the parent id is null)
     *
     * @param  Builder      $query      Original query
     * @return Builder                  Modified query
     */
    public function scopeRootNodes($query)
    {
        return $query->whereNull($this->getParentIdColumnName());
    }

    /**
     * Query filter to return only the root node for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find root of
     * @return Builder                  Modified query
     */
    public function scopeHierarchyRootOf($query, IsHierarchicalContract $node)
    {
        return $query
            ->where($node->getLeftIdColumnName(), '<', $node->getHierarchyLeftId())
            ->where($node->getRightIdColumnName(), '>', $node->getHierarchyRightId())
            ->whereNull($node->getParentIdColumnName());
    }

    /**
     * Query filter to return only the parent node for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find parent of
     * @return Builder                  Modified query
     */
    public function scopeHierarchyParentOf($query, IsHierarchicalContract $node)
    {
        return $query->where($node->getKeyName(), $node->getHierarchyParentId());
    }

    /**
     * Query filter to return only the sibling nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find siblings of
     * @return Builder                  Modified query
     */
    public function scopeHierarchySiblingsOf($query, IsHierarchicalContract $node)
    {
        return $query
            ->where($node->getParentIdColumnName(), $node->getHierarchyParentId())
            ->where($node->getKeyName(), '<>', $node->getKey());
    }

    /**
     * Query filter to return only the child nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find childrens of
     * @return Builder                  Modified query
     */
    public function scopeHierarchyChildrenOf($query, IsHierarchicalContract $node)
    {
        return $query->where($node->getParentIdColumnName(), $node->getKey());
    }

    /**
     * Query filter to return only the ascendant nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find ascendants of
     * @return Builder                  Modified query
     */
    public function scopeHierarchyAscendantsOf($query, IsHierarchicalContract $node)
    {
        return $query
            ->where($node->getLeftIdColumnName(), "<", $node->getHierarchyLeftId())
            ->where($node->getRightIdColumnName(), ">", $node->getHierarchyRightId());
    }

    /**
     * Query filter to return only the descendant nodes for the given node
     *
     * @param  Builder          $query  Original query
     * @param  IsHierarchical   $node   Node to find descendants of
     * @return Builder                  Modified query
     */
    public function scopeHierarchyDescendantsOf($query, IsHierarchicalContract $node)
    {
        return $query->whereBetween($node->getLeftIdColumnName(), [
            ($node->getHierarchyLeftId() + 1),
            ($node->getHierarchyRightId() - 1)
        ]);
    }
}
