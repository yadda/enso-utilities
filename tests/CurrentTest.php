<?php

use Yadda\Enso\Utilities\Hierarchy\Repositories\HierarchyRepository;

/**
 * Tests that a node can be 'putUnder' a node to the left of it's current
 * position in the hierarchy tree, testing various constraints and conditions
 *
 * @return void
 */
class TestCurrent extends TestCase
{
    /**
     * Tests previously discovered issue with moving an item under another item
     * that makes it's new position a sibling of it's previous parent.
     *
     * @return void
     */
    public function test_put_before_sibling_switch()
    {
        $this->generateComplexNodeTree();
    }
}
