<?php

use Yadda\Enso\Utilities\Hierarchy\Repositories\HierarchyRepository;
use Yadda\Enso\Utilities\Tests\Models\Role;

use Yadda\Enso\Utilities\Tests\TestCase;

class HierarchicalQueryBuilderTests extends TestCase
{
    // // Testing use cases
    // public function test_that_a_new_node_can_be_created_into_an_empty_tree() {
    //     $new_node = HierarchyRepository::create(Role::class, [
    //         'name' => 'Test 1',
    //         'slug' => 'test-1',
    //         'description' => ''
    //     ]);

    //     $this->assertNull($new_node->getHierarchyParentId());
    //     $this->assertEquals($new_node->getHierarchyLeftId(), 1);
    //     $this->assertEquals($new_node->getHierarchyRightId(), 2);
    //     $this->assertEquals($new_node->getHierarchyDepth(), 1);
    // }

    // public function test_that_a_new_root_node_can_be_created() {
    //     $this->generateComplexNodeTree();

    //     $new_node = HierarchyRepository::create(Role::class, [
    //         'name' => 'Test 1',
    //         'slug' => 'test-1',
    //         'description' => 'Nope'
    //     ]);

    //     $this->assertNull($new_node->getHierarchyParentId());
    //     $this->assertEquals($new_node->getHierarchyLeftId(), 35);
    //     $this->assertEquals($new_node->getHierarchyRightId(), 36);
    //     $this->assertEquals($new_node->getHierarchyDepth(), 1);
    // }

    // // Testing actual callable functions
    // public function test_the_make_root_node_functionality()
    // {
    //     $this->generateComplexNodeTree();

    //     $this->nodes[12]->convertToRootNode();

    //     $this->refreshTree();

    //     $this->assertNull($this->nodes[12]->getHierarchyParentId());
    //     $this->assertEquals($this->nodes[12]->getHierarchyLeftId(), 27);
    //     $this->assertEquals($this->nodes[12]->getHierarchyRightId(), 34);
    //     $this->assertEquals($this->nodes[12]->getHierarchyDepth(), 1);
    // }
}
