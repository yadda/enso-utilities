<?php

namespace Yadda\Enso\Utilities\Tests\Hierarchy\PutBefore;

use Yadda\Enso\Utilities\Tests\TestCase;

/**
 * Tests that a node can be 'putUnder' a node to the left of it's current
 * position in the hierarchy tree, testing various constraints and conditions
 *
 * @return void
 */
class HierarchyQueryBuilderPutBeforeRightTests extends TestCase
{
    // /**
    //  * Tests that a node with no children can be moved right to be a child of a
    //  * node with no children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_before_functionality_moving_right_with_no_children_and_no_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with no children under another node with no children
    //     $this->assertTrue($this->nodes[5]->putBefore($this->nodes[15]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[5]->getHierarchyParentId(), 14);
    //     $this->assertEquals($this->nodes[5]->getHierarchyLeftId(), 26);
    //     $this->assertEquals($this->nodes[5]->getHierarchyRightId(), 27);
    //     $this->assertEquals($this->nodes[5]->getHierarchyDepth(), 4);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root left id updated but not right id
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 21);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent node left id updated but not right_id
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 23);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 30);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[8]->getHierarchyLeftId(), 13);
    //     $this->assertEquals($this->nodes[8]->getHierarchyRightId(), 14);

    //     // Check root of move FROM node is update right id but not left
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 10);

    //     // Check parent of move FROM node is update right id but not left
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 5);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 8);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with children can be moved right to be a child of a
    //  * node with no children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_before_functionality_moving_right_with_children_and_no_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with chilren under a node with no children.
    //     $this->assertTrue($this->nodes[3]->putBefore($this->nodes[15]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[3]->getHierarchyParentId(), 14);
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 22);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 27);
    //     $this->assertEquals($this->nodes[3]->getHierarchyDepth(), 4);

    //     // Check Child of moved node is correct
    //     $this->assertEquals($this->nodes[5]->getHierarchyParentId(), 4);
    //     $this->assertEquals($this->nodes[5]->getHierarchyLeftId(), 25);
    //     $this->assertEquals($this->nodes[5]->getHierarchyRightId(), 26);
    //     $this->assertEquals($this->nodes[5]->getHierarchyDepth(), 5);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root left id updated but not right id
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 17);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent node left id updated but not right_id
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 19);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 30);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[8]->getHierarchyLeftId(), 9);
    //     $this->assertEquals($this->nodes[8]->getHierarchyRightId(), 10);

    //     // Check root of move FROM node is update right id but not leftleft
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 6);

    //     // Check parent of move FROM node is update right id but not leftleft
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 5);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with no children can be moved right to be a child of a
    //  * node with children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_before_functionality_moving_right_with_no_children_and_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with no children under another node with no children
    //     $this->assertTrue($this->nodes[5]->putBefore($this->nodes[13]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[5]->getHierarchyParentId(), 13);
    //     $this->assertEquals($this->nodes[5]->getHierarchyLeftId(), 23);
    //     $this->assertEquals($this->nodes[5]->getHierarchyRightId(), 24);
    //     $this->assertEquals($this->nodes[5]->getHierarchyDepth(), 3);

    //     // Check node to the leftleftleft of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root right id updated but not right id
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 21);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent node right id updated but not right_id
    //     $this->assertEquals($this->nodes[12]->getHierarchyLeftId(), 22);
    //     $this->assertEquals($this->nodes[12]->getHierarchyRightId(), 31);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[8]->getHierarchyLeftId(), 13);
    //     $this->assertEquals($this->nodes[8]->getHierarchyRightId(), 14);

    //     // Check root of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 10);

    //     // Check parent of move FROM node is update left id but not right
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 5);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 8);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }

    // /**
    //  * Tests that a node with children can be moved right to be a child of a
    //  * node with children
    //  *
    //  * @return void
    //  */
    // public function test_the_put_before_functionality_moving_right_with_children_and_sibling_children()
    // {
    //     $this->generateComplexNodeTree();

    //     // Move a node with chilren under a node with no children.
    //     $this->assertTrue($this->nodes[3]->putBefore($this->nodes[13]));

    //     // Reload the tree so to make checks that the appropriate nodes around
    //     // and between the change sites are correct
    //     $this->refreshTree();

    //     // Check moved node is correct
    //     $this->assertEquals($this->nodes[3]->getHierarchyParentId(), 13);
    //     $this->assertEquals($this->nodes[3]->getHierarchyLeftId(), 19);
    //     $this->assertEquals($this->nodes[3]->getHierarchyRightId(), 24);
    //     $this->assertEquals($this->nodes[3]->getHierarchyDepth(), 3);

    //     // Check Child of moved node is correct
    //     $this->assertEquals($this->nodes[5]->getHierarchyParentId(), 4);
    //     $this->assertEquals($this->nodes[5]->getHierarchyLeftId(), 22);
    //     $this->assertEquals($this->nodes[5]->getHierarchyRightId(), 23);
    //     $this->assertEquals($this->nodes[5]->getHierarchyDepth(), 4);

    //     // Check node to the left of change sites is unchanged.
    //     $this->assertEquals($this->nodes[1]->getHierarchyLeftId(), 2);
    //     $this->assertEquals($this->nodes[1]->getHierarchyRightId(), 3);

    //     // Check root left id updated but not right id
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 17);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 34);

    //     // Check parent node left id updated but not right_id
    //     $this->assertEquals($this->nodes[12]->getHierarchyLeftId(), 18);
    //     $this->assertEquals($this->nodes[12]->getHierarchyRightId(), 31);

    //     // Check node fully between change sites updated
    //     $this->assertEquals($this->nodes[8]->getHierarchyLeftId(), 9);
    //     $this->assertEquals($this->nodes[8]->getHierarchyRightId(), 10);

    //     // Check root of move FROM node is update right id but not left
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 6);

    //     // Check parent of move FROM node is update right id but not left
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 5);

    //     // Check node to the right of change sites is unchanged
    //     $this->assertEquals($this->nodes[16]->getHierarchyLeftId(), 32);
    //     $this->assertEquals($this->nodes[16]->getHierarchyRightId(), 33);
    // }
}
