<?php

use Yadda\Enso\Utilities\Hierarchy\Exceptions\HierarchicalNodeHasChildrenException;

use Yadda\Enso\Utilities\Hierarchy\Repositories\HierarchyRepository;
use Yadda\Enso\Utilities\Tests\Models\Role;

use Yadda\Enso\Utilities\Tests\TestCase;

class HierarchicalQueryBuilderDeleteTests extends TestCase
{
    // public function test_that_a_node_can_be_deleted() {
    //     $this->generateComplexNodeTree();

    //     $this->assertEquals(Role::count(), 17);

    //     HierarchyRepository::delete($this->nodes[14]);

    //     // Check node count
    //     $this->assertEquals(Role::count(), 16);

    //     // Check node deleted
    //     $this->assertNull(Role::find($this->nodes[14]->id));

    //     $this->refreshTree();

    //     // Check parent children count
    //     $this->assertEquals($this->nodes[13]->childNodes()->count(), 1);

    //     // Check previous sibling has correct left + right ids
    //     $this->assertEquals($this->nodes[14]->getHierarchyLeftId(), 26);
    //     $this->assertEquals($this->nodes[14]->getHierarchyRightId(), 27);

    //     // Check other node to the righth has correct right and left ids
    //     $this->assertEquals($this->nodes[15]->getHierarchyLeftId(), 30);
    //     $this->assertEquals($this->nodes[15]->getHierarchyRightId(), 31);

    //     // Check parent left and right ids
    //     $this->assertEquals($this->nodes[13]->getHierarchyLeftId(), 25);
    //     $this->assertEquals($this->nodes[13]->getHierarchyRightId(), 28);

    //     // Check root left and right ids
    //     $this->assertEquals($this->nodes[11]->getHierarchyLeftId(), 23);
    //     $this->assertEquals($this->nodes[11]->getHierarchyRightId(), 32);
    // }

    // public function test_that_a_node_with_children_cant_be_deleted()
    // {
    //     $this->generateComplexNodeTree();

    //     $this->setExpectedException(HierarchicalNodeHasChildrenException::class);
    //     HierarchyRepository::delete($this->nodes[12]);
    // }

    // // Testing actual callable functions
    // public function test_that_a_branch_can_be_deleted()
    // {
    //     $this->generateComplexNodeTree();

    //     $this->assertEquals(Role::count(), 17);

    //     HierarchyRepository::delete($this->nodes[3], true);

    //     // Check node count
    //     $this->assertEquals(Role::count(), 14);

    //     // Check node deleted
    //     $this->assertNull(Role::find($this->nodes[3]->id));

    //     $this->refreshTree();

    //     // Check parent children count
    //     $this->assertEquals($this->nodes[2]->childNodes()->count(), 0);

    //     // Check other node to the righth has correct right and left ids
    //     $this->assertEquals($this->nodes[4]->getHierarchyLeftId(), 8);
    //     $this->assertEquals($this->nodes[4]->getHierarchyRightId(), 13);

    //     // Check parent left and right ids
    //     $this->assertEquals($this->nodes[2]->getHierarchyLeftId(), 4);
    //     $this->assertEquals($this->nodes[2]->getHierarchyRightId(), 5);

    //     // Check root left and right ids
    //     $this->assertEquals($this->nodes[0]->getHierarchyLeftId(), 1);
    //     $this->assertEquals($this->nodes[0]->getHierarchyRightId(), 6);
    // }
}
