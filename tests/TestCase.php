<?php

namespace Yadda\Enso\Utilities\Tests;

use Illuminate\Contracts\Console\Kernel;
use Illuminate\Filesystem\Filesystem;
use Orchestra\Testbench\TestCase as BaseTestCase;
use Yadda\Enso\Utilities\EnsoUtilitiesServiceProvider;
use Yadda\Enso\Utilities\Tests\Models\Role;

abstract class TestCase extends BaseTestCase
{
    // /**
    //  * The base URL to use while testing the application.
    //  *
    //  * @var string
    //  */
    // protected $baseUrl = 'http://localhost';

    // protected $nodes;

    // /**
    //  * Creates the application.
    //  *
    //  * @return \Illuminate\Foundation\Application
    //  */
    // public function createApplication()
    // {
    //     $app = require __DIR__.'/../vendor/laravel/laravel/bootstrap/app.php';

    //     $app->register(EnsoUtilitiesServiceProvider::class);

    //     $app->make(Kernel::class)->bootstrap();

    //     return $app;
    // }

    // /**
    //  * Add the parent setUp function by setting up an in-memory sqlite db
    //  * and then running all the migrations in this package
    //  */
    // public function setUp(): void
    // {
    //     parent::setUp();

    //     // Database setup
    //     $this->app['config']->set('database.default', 'mysql');
    //     $this->app['config']->set('database.connections.mysql.database', 'testing');
    //     $this->app['config']->set('database.connections.mysql.username', 'yadda');
    //     $this->app['config']->set('database.connections.mysql.password', 'marmite2012');

    //     $this->migrate();
    // }

    // /**
    //  * Run package database migrations
    //  *
    //  * @return void
    //  */
    // public function migrate()
    // {
    //     $fileSystem = new Filesystem;

    //     // @todo - rework automated testing

    //     // $classFinder = new ClassFinder;

    //     // foreach ($fileSystem->files(__DIR__ . "/database/migrations") as $file) {
    //     //     $fileSystem->requireOnce($file);

    //     //     $migrationClass = $classFinder->findClass($file);

    //     //     (new $migrationClass)->up();
    //     // }
    // }

    /**
     * Create a simple node tree to run tests on
     *
     * @return void
     */
    protected function generateSimpleNodeTree()
    {
        $this->nodes[] = Role::forceCreate([
            'id' => 1,
            'name' => 'Root 1',
            'slug' => 'r1',
            'parent_id' => null,
            'description' => '',
            'left_id' => 1,
            'right_id' => 8,
            'depth' => 1
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 2,
            'name' => 'Child 1',
            'slug' => 'r1-c1',
            'parent_id' => 1,
            'description' => '',
            'left_id' => 2,
            'right_id' => 7,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 3,
            'name' => 'Child 1',
            'slug' => 'r1-c1-c1',
            'parent_id' => 2,
            'description' => '',
            'left_id' => 3,
            'right_id' => 4,
            'depth' => 3
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 4,
            'name' => 'Child 2',
            'slug' => 'r1-c1-c2',
            'parent_id' => 2,
            'description' => '',
            'left_id' => 5,
            'right_id' => 6,
            'depth' => 3
        ]);
    }

    /**
     * Creates a complex node tree to run tests on
     *
     * @return void
     */
    protected function generateComplexNodeTree()
    {
        $this->nodes[] = Role::forceCreate([
            'id' => 1,
            'name' => 'Root 1',
            'slug' => 'r1',
            'parent_id' => null,
            'description' => '',
            'left_id' => 1,
            'right_id' => 12,
            'depth' => 1
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 2,
            'name' => 'Child 1',
            'slug' => 'r1-c1',
            'parent_id' => 1,
            'description' => '',
            'left_id' => 2,
            'right_id' => 3,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 3,
            'name' => 'Child 2',
            'slug' => 'r1-c2',
            'parent_id' => 1,
            'description' => '',
            'left_id' => 4,
            'right_id' => 11,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 4,
            'name' => 'Child 1',
            'slug' => 'r1-c1-c1',
            'parent_id' => 3,
            'description' => '',
            'left_id' => 5,
            'right_id' => 10,
            'depth' => 3
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 5,
            'name' => 'Child 1',
            'slug' => 'r1-c1-c1-c1',
            'parent_id' => 4,
            'description' => '',
            'left_id' => 6,
            'right_id' => 7,
            'depth' => 4
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 6,
            'name' => 'Child 2',
            'slug' => 'r1-c1-c1-c2',
            'parent_id' => 4,
            'description' => '',
            'left_id' => 8,
            'right_id' => 9,
            'depth' => 4
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 7,
            'name' => 'Root 2',
            'slug' => 'r2',
            'parent_id' => null,
            'description' => '',
            'left_id' => 13,
            'right_id' => 22,
            'depth' => 1
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 8,
            'name' => 'Child 1',
            'slug' => 'r2-c1',
            'parent_id' => 7,
            'description' => '',
            'left_id' => 14,
            'right_id' => 19,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 9,
            'name' => 'Child 1',
            'slug' => 'r2-c1-c1',
            'parent_id' => 8,
            'description' => '',
            'left_id' => 15,
            'right_id' => 16,
            'depth' => 3
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 10,
            'name' => 'Child 2',
            'slug' => 'r2-c1-c2',
            'parent_id' => 8,
            'description' => '',
            'left_id' => 17,
            'right_id' => 18,
            'depth' => 3
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 11,
            'name' => 'Child 2',
            'slug' => 'r2-c2',
            'parent_id' => 7,
            'description' => '',
            'left_id' => 20,
            'right_id' => 21,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 12,
            'name' => 'Root 3',
            'slug' => 'r3',
            'parent_id' => null,
            'description' => '',
            'left_id' => 23,
            'right_id' => 34,
            'depth' => 1
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => 13,
            'name' => 'Child 1',
            'slug' => 'r3-c1',
            'parent_id' => 12,
            'description' => '',
            'left_id' => 24,
            'right_id' => 31,
            'depth' => 2
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => '14',
            'name' => 'Child 1',
            'slug' => 'r3-c1-c1',
            'parent_id' => 13,
            'description' => '',
            'left_id' => 25,
            'right_id' => 30,
            'depth' => 3
        ]);

        $this->nodes[] = Role::forceCreate([
            'id' => '15',
            'name' => 'Child 1',
            'slug' => 'r3-c1-c1-c1',
            'parent_id' => 14,
            'description' => '',
            'left_id' => 26,
            'right_id' => 27,
            'depth' => 4
        ]);
        $this->nodes[] = Role::forceCreate([
            'id' => '16',
            'name' => 'Child 2',
            'slug' => 'r3-c1-c1-c2',
            'parent_id' => 14,
            'description' => '',
            'left_id' => 28,
            'right_id' => 29,
            'depth' => 4
        ]);
        $this->nodes[] = Role::forceCreate([
            'id' => '17',
            'name' => 'Child 2',
            'slug' => 'r3-c2',
            'parent_id' => 12,
            'description' => '',
            'left_id' => 32,
            'right_id' => 33,
            'depth' => 2
        ]);
    }

    /**
     * Pulls a fresh copy of the node tree for the purposes of running tests
     *
     * @return void
     */
    protected function refreshTree()
    {
        $fresh_nodes = Role::all();

        foreach ($this->nodes as $index => $node) {
            $this->nodes[$index] = $fresh_nodes->get($index);
        }
    }
}
